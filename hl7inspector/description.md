﻿The HL7 Inspector is a useful open source tool for integration the HL7 Version 2 in a health care environmental.
It will help you to minimize the time for tuning the HL7 communication between systems such 
as HIS and RIS by analyzing and validating HL7 messages. 

###Features

* Parsing of HL7 messages
* Sending/receiving of messages on IP sockets
* Reading of HL7 messages from (compressed) text/log files
* Simple message editor
* Full Unicode support
* ...and many more

For a complete list please go to https://bitbucket.org/crambow/hl7inspector/src/master/release-notes.md
