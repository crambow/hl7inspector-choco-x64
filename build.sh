#!/usr/bin/env bash

PACKAGE=$1
VERSION=$2
DOWNLOAD_ROOT="https://files.hl7inspector.com/stable"
AUTHOR="Carsten Rambow"
TARGET_PATH=./target
#$author = (git config user.name).Trim()

if [[ $PACKAGE == "" ]]; then
  echo "The parameter PACKAGE is missing. Set testing alternative."
  PACKAGE="hl7inspector.install"
fi

if [[ $VERSION == "" ]]; then
  echo "The parameter VERSION is missing. Set testing alternative."
  VERSION="0.0.0.1-SNAPSHOT"
fi

SOURCE_PATH=./$PACKAGE

echo
echo "Package: $PACKAGE"
echo "Version: $VERSION"
echo "Source Path: $SOURCE_PATH"
echo "Target Path: $TARGET_PATH"
echo

#ls -l

# ----------------------------------------------------------------------------------------------------------------
# Cleaning or creating target/choco folder
echo "* Cleaning target folder..."
rm -f -r $TARGET_PATH
mkdir $TARGET_PATH

# ----------------------------------------------------------------------------------------------------------------
# Download checksum
echo "* Downloading SHA256 checksum of setup package..."
FILE_SHA256="`wget -qO- $DOWNLOAD_ROOT/v$VERSION/HL7+Inspector+$VERSION-x64+Setup.sha256`"
if [[ $FILE_SHA256 == "" ]]; then
  echo "! WARNING: No SHA256 checksum found. Set test alternative."
  FILE_SHA256="01234567890abcdef"
fi
echo "  File SHA256 checksum: $FILE_SHA256"

# ----------------------------------------------------------------------------------------------------------------
# Assembly files for cinst
echo "* Assembling files for Chocolatey packing..."
cp -r $SOURCE_PATH $TARGET_PATH

NUSPEC="$TARGET_PATH/$PACKAGE/$PACKAGE.nuspec"
INSTALL_PS="$TARGET_PATH/$PACKAGE/tools/chocolateyinstall.ps1"

sed -i "s/#version#/${VERSION}/g" $NUSPEC
sed -i "s/#author#/${AUTHOR}/g" $NUSPEC

sed -i "s/#version#/${VERSION}/" $INSTALL_PS
sed -i "s/#fileSha256#/${FILE_SHA256}/" $INSTALL_PS

# ----------------------------------------------------------------------------------------------------------------
# Pack and push files

cd target/$PACKAGE || exit

ls -l

echo "* Packaging Chocolatey files..."
choco pack

if [[ $VERSION == *"SNAPSHOT" ]]; then
  echo "! SNAPSHOT's not supported by Chocolatey. So we have to skip 'choco push'."
  exit
fi

if [[ $FILE_SHA256 == "01234567890"* ]]; then
  echo "! Use alternative SHA256 checksum. Makes no sense to push files. So we have to skip 'choco push'."
  exit
fi

echo "* Pushing Chocolatey files..."
choco push --apiKey "$CHOCO_KEY"

echo "* Done. Please check results."