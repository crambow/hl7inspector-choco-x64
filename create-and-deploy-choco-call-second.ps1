# Script backup in case when Linux env doesn't work anymore

Write-Host "=====================================================" -ForegroundColor Yellow
Write-Host "Building and deploy choco distribution of" -ForegroundColor Gray
Write-Host " _   _  __    ___" -ForegroundColor DarkGray
Write-Host "( )_( )(  )  (__ )" -ForegroundColor DarkGray
Write-Host " ) _ (  )(__  / /" -ForegroundColor DarkGray
Write-Host "(_) (_)(____)(_/" -ForegroundColor DarkGray
Write-Host " ____  _  _  ___  ____  ____  ___  ____  _____  ____" -ForegroundColor DarkGray
Write-Host "(_  _)( \( )/ __)(  _ \( ___)/ __)(_  _)(  _  )(  _ \" -ForegroundColor DarkGray
Write-Host " _)(_  )  ( \__ \ )___/ )__)( (__   )(   )(_)(  )   /" -ForegroundColor DarkGray
Write-Host "(____)(_)\_)(___/(__)  (____)\___) (__) (_____)(_)\_)" -ForegroundColor DarkGray
Write-Host '=====================================================' -ForegroundColor Yellow
Write-Host ""

$targetPath = '.\target'
$targetChoco = "$targetPath\choco"
$sourceHli = '.\hl7inspector'

$nugetPath = "$targetChoco\hl7inspector"
$nugetFile = "$nugetPath\hl7inspector.nuspec"

$author = (git config user.name).Trim()
# ----------------------------------------------------------------------------------------------------------------
# Asking for version
$version = Read-Host -Prompt 'Enter HL7 Inspector version to choconize'
$deploy = Read-Host -Prompt 'Deploy packages to Chocolatey.com (Type Yes when Yes)'

# ----------------------------------------------------------------------------------------------------------------
# Cleaning or creating target/choco folder
Write-Host "Cleaning/creating target choco folder '$targetChoco'..." -ForegroundColor Yellow
#Get-ChildItem -Path $targetChoco -Recurse | Remove-Item -force -recurse
Remove-Item -LiteralPath "$nugetPath" -Force -Recurse
New-Item -ItemType Directory -Force -Path "$targetChoco"

# ----------------------------------------------------------------------------------------------------------------
# Assembly files for cinst
Write-Host "Assembling and patching files for 'choco pack'..." -ForegroundColor Yellow
Copy-Item -Path $sourceHli -Destination "$targetChoco" -recurse -Force

Write-Host "Patching file '$nugetFile'..." -ForegroundColor DarkGray
$description = Get-Content "$sourceHli\description.md"

(Get-Content $nugetFile).replace('#version#', "$version") | Set-Content $nugetFile
(Get-Content $nugetFile).replace('#author#', "$author") | Set-Content $nugetFile
(Get-Content $nugetFile).replace('#description#', "$description") | Set-Content $nugetFile

# ----------------------------------------------------------------------------------------------------------------
# Choco pack
Write-Host "Packing NuSpec files..." -ForegroundColor Yellow
cpack "$nugetFile" --out "$nugetPath" --version=$version
if (!$?) {
    Write-Host "Last execute failed. Exit script." -ForegroundColor Red
    exit $?
}

# ----------------------------------------------------------------------------------------------------------------
# Ask for deploy choco or skip

# ----------------------------------------------------------------------------------------------------------------
# Push choco

if ($deploy -eq "Yes") {
    Write-Host "Pushing NuSpec files..." -ForegroundColor Yellow
    cpush "$nugetPath"
} else {
    Write-Host "Skipping choco push" -ForegroundColor Yellow
}

if (!$?) {
    Write-Host "Last execute failed. Exit script." -ForegroundColor Red
    exit $?
}
