# Script backup in case when Linux env doesn't work anymore

Write-Host "=====================================================" -ForegroundColor Yellow
Write-Host "Building and deploy choco INSTALL distribution of" -ForegroundColor Gray
Write-Host " _   _  __    ___" -ForegroundColor DarkGray
Write-Host "( )_( )(  )  (__ )" -ForegroundColor DarkGray
Write-Host " ) _ (  )(__  / /" -ForegroundColor DarkGray
Write-Host "(_) (_)(____)(_/" -ForegroundColor DarkGray
Write-Host " ____  _  _  ___  ____  ____  ___  ____  _____  ____" -ForegroundColor DarkGray
Write-Host "(_  _)( \( )/ __)(  _ \( ___)/ __)(_  _)(  _  )(  _ \" -ForegroundColor DarkGray
Write-Host " _)(_  )  ( \__ \ )___/ )__)( (__   )(   )(_)(  )   /" -ForegroundColor DarkGray
Write-Host "(____)(_)\_)(___/(__)  (____)\___) (__) (_____)(_)\_)" -ForegroundColor DarkGray
Write-Host '=====================================================' -ForegroundColor Yellow
Write-Host ""

$downloadRoot = 'https://files.hl7inspector.com/stable';

$targetPath = '.\target'
$targetChoco = "$targetPath\choco"
$sourceHliInstall = '.\hl7inspector.install'

$nugetInstallPath = "$targetChoco\hl7inspector.install"
$nugetInstallFile = "$nugetInstallPath\hl7inspector.install.nuspec"

$author = (git config user.name).Trim()
# ----------------------------------------------------------------------------------------------------------------
# Asking for version
$version = Read-Host -Prompt 'Enter HL7 Inspector version to choconize'
$deploy = Read-Host -Prompt 'Deploy packages to Chocolatey.com (Type Yes when Yes)'

# ----------------------------------------------------------------------------------------------------------------
# Cleaning or creating target/choco folder
Write-Host "Cleaning/creating target choco folder '$targetChoco'..." -ForegroundColor Yellow
#Get-ChildItem -Path $targetChoco -Recurse | Remove-Item -force -recurse
Remove-Item -LiteralPath "$nugetInstallPath" -Force -Recurse
New-Item -ItemType Directory -Force -Path "$targetChoco"

# ----------------------------------------------------------------------------------------------------------------
# Download checksum
$setup_sha256_url = "$downloadRoot/v$version/HL7%20Inspector%20$version-x64%20Setup.sha256"
Write-Host "Downloading checksum file '$setup_sha256_url'..." -ForegroundColor Yellow
$filename_wo_ext = "HL7 Inspector $version-x64 Setup"
$ProgressPreference = 'SilentlyContinue'
Invoke-WebRequest -Uri "$setup_sha256_url" -OutFile "$targetChoco\$filename_wo_ext.sha256"
if (!$?) {
    Write-Host "Download failed. Exit script." -ForegroundColor Red
    exit $?
}
$fileSha256 = (Get-Content "$targetChoco\$filename_wo_ext.sha256").Trim()
Write-Host "SHA256: $fileSha256" -ForegroundColor DarkGray

# ----------------------------------------------------------------------------------------------------------------
# Assembly files for cinst
Write-Host "Assembling and patching files for 'choco pack'..." -ForegroundColor Yellow
Copy-Item -Path $sourceHliInstall -Destination "$targetChoco" -recurse -Force

Write-Host "Patching file '$nugetInstallFile'..." -ForegroundColor DarkGray
$description = Get-Content "$sourceHliInstall\description.md"

(Get-Content $nugetInstallFile).replace('#version#', "$version") | Set-Content $nugetInstallFile
(Get-Content $nugetInstallFile).replace('#author#', "$author") | Set-Content $nugetInstallFile
(Get-Content $nugetInstallFile).replace('#description#', "$description") | Set-Content $nugetInstallFile

$nugetInstallScriptFile = "$nugetInstallPath\tools\chocolateyinstall.ps1"
Write-Host "Patching file '$nugetInstallScriptFile'..." -ForegroundColor DarkGray
(Get-Content $nugetInstallScriptFile).replace('#version#', "$version") | Set-Content $nugetInstallScriptFile
(Get-Content $nugetInstallScriptFile).replace('#fileSha256#', "$fileSha256") | Set-Content $nugetInstallScriptFile

# ----------------------------------------------------------------------------------------------------------------
# Choco pack
Write-Host "Packing NuSpec files..." -ForegroundColor Yellow
cpack "$nugetInstallFile" --out "$nugetInstallPath" --version=$version
if (!$?) {
    Write-Host "Last execute failed. Exit script." -ForegroundColor Red
    exit $?
}

# ----------------------------------------------------------------------------------------------------------------
# Push choco

if ($deploy -eq "Yes") {
    Write-Host "Pushing NuSpec files..." -ForegroundColor Yellow
    cpush "$nugetInstallPath"
} else {
    Write-Host "Skipping choco push" -ForegroundColor Yellow
}

if (!$?) {
    Write-Host "Last execute failed. Exit script." -ForegroundColor Red
    exit $?
}
